# Arduino
Coisas para meu arduino

## Instalando o arduino no Linux

Baixe o arquivo da pagina oficial [www.arduino.org](www.arduino.org), lembre-se da arquitetura do processador do seu pc/notebook.

Descompacte o aquivo, em seguinda entre na pasta pelo termnal.

```Shell
cd pasta_criada
```

Lá dentro terpa uma pasta com o nome "arduino-versão" Ex.: arduino-1.8.10.  
Vamos copiar essa pasta para o diretório do linux `opt`, eu já fiz a cópia substiruindo o nome da pasta para arduino. lembre-se de fazer como super usuário (su ou sudo).

```Shell
# cp arduino-1.8.10/ /opt/arduino
```

Agora vamos entrar na pasta `opt/` para rodar o install.sh (lembra do super usuario):

```Shell

cd /opt/

# ./install.sh
```

Esse comando irár criar os atalhos, itens no menu e apontar para a pasta dentro de `opt/`, assim, pode baixar o arquivo baixado e a pasta criada ao descompactar o arquivo.

## Liberando a porta para ler/gravar sketches no arduino

> Retirado do site oficial do [Arduino](www.arduindo.org)

Caso, ao tentar gravar algum schetche, ele dará um erro parecido com esse:

```
Error opening serial port ....... tty<alguma coisa>
```

Isso significa que seu usuário não tem permissão de leitura e escrita em tal porta, sendo que `tty<Algumacoisa>` indica em qual porta não tem a permissão.  
Nesse caso basta rodar o seguinte comando no terminal:

```Shell
sudo usermod -a -G dialout <nome do usuário> # Substitua <nome do usuário> pelo nome do usuário que deseja permitir
```

Faça um logout e login para que as permissões tenham efeito.

## Usando o VSCODE para programar

[Tutorial da Embarcados](https://www.embarcados.com.br/arduino-vscode-platformio/)

## POO para Arduino

[Filipeflop](https://www.filipeflop.com/blog/utilizando-a-programacao-orientada-a-objeto-para-arduino/)
